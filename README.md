# SocialShare

This is the gitlab page for the social media application "SocialShare" that was created to learn about concepts like session ids, cookies and the MERN stack.
It may later be deployed but this is not yet planned as it was developed for educational purposes.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The programs needed to launch this project. Installation varies between OSs

1. NodeJS
2. MongoDB


### Installing

Use the README.md files in the backend and frontend folder to install and launch the development project.

## Built With

* [Node.js](https://nodejs.org/en/) - The web framework used
* [Express.js](https://expressjs.com/) - Web framework for Node.js
* [MongoDB](https://www.mongodb.com/) - The NOSQL database
* [React](https://reactjs.org/) - The website framework



## Authors

* **xCubeArrow** - *Initial work* - [xCubeArrow](https://github.com/xCubeArrow)

See also the list of [contributors](https://github.com/xcubearrow/social-share/graphs/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details
