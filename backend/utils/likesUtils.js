const User = require("../modules/user");
const Post = require("../modules/post");


const printPostLikes = (post, userId) => {
    let result = post.toJSON();
    result.userLiked = false;
    result.userDisliked = false;
    if (post.likesUsers.includes(userId)) {
        result.userLiked = true
    }
    if (post.dislikesUsers.includes(userId)) {
        result.userDisliked = true
    }
    result.dislikesUsers = null;
    result.likesUsers = null;

    return result
};

const followOrUnfollow = async (user, userToFollow) => {
    if (!user.followers.includes(userToFollow._id)) {
        userToFollow.followercount += 1;
        user.followers.push(userToFollow._id)
    } else {
        userToFollow.followercount -= 1;
        user.followers.splice(user.followers.indexOf(userToFollow._id))
    }
    return [user, userToFollow]
};

const likeOrDislike = (likesUsers, likes, userId) => {
    if (!likesUsers.includes(userId)) {
        likes = likes + 1;
        likesUsers.push(userId)
    } else {
        likes = likes - 1;
        likesUsers.splice(likesUsers.indexOf(userId))
    }
    return [likesUsers, likes]
};

const postLikeOrUnlike = async (postId, userSid, like) => {
    const post = await Post.findById(postId);
    const userId = (await User.findOne({
        sid: userSid
    }))._id;
    if(like){
        const result = likeOrDislike(post.likesUsers, post.likes, userId);
        post.likesUsers = result[0];
        post.likes = result[1];
    } else {
        const result = likeOrDislike(post.dislikesUsers, post.dislikes, userId);
        post.dislikesUsers = result[0];
        post.dislikes = result[1];
    }
    await post.save();
    return printPostLikes(post, userId);
};

module.exports = {printPostLikes, likeOrDislike, followOrUnfollow, postLikeOrUnlike};