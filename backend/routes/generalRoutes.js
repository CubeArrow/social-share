require("dotenv").config({path: __dirname + '../.env'});
const express = require("express");
const mongoose = require("mongoose");

const router = express.Router();

router.get("/currentTheme", (req, res) => {
    res.json({theme: process.env.CURRENT_THEME})
});

module.exports = router;