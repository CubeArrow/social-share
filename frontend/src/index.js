import React from "react"
import ReactDOM from "react-dom"
const BackendConnector = require("./utils/BackendConnector");


BackendConnector.getCurrentTheme().then((theme) => {
    console.log(theme)
    const Index = require("./themes/" + theme + "/index").default;
    ReactDOM.render(<Index />, document.getElementById("root"));
});
