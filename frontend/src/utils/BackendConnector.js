let uri = "http://localhost:2020/api";

export const getCurrentTheme = async () => {
    const result = await fetch(uri + "/currentTheme", {
        method: "GET",
        credentials: "include",
        mode: "cors"
    });
    return (await result.json()).theme;
};

export const likeComment = (id) => {
    fetch(uri + "/comment/like/" + id,
        {method: "POST", credentials: "include"}).then(() => window.location.reload());
};

export const submitPost = (title, fileName) => {
    if (fileName) {
        const fr = new FileReader();
        fr.readAsDataURL(fileName);
        fr.onload = () => {
            const json = JSON.stringify({
                "title": title,
                "data": encodeURI(fr.result)
            });
            fetch(uri + "/post", {
                method: "post",
                body: json,
                credentials: "include",
                headers: {'Content-type': 'application/json'}

            }).then((response) => console.log(response))
        }
    } else {
        fetch(uri + "/post", {
            method: "post",
            body: {title: title},
            credentials: "include",
            headers: {'Content-type': 'application/json'}

        }).then((response) => console.log(response))
    }
};

export const getComments = async (postId) => {
    const comments = await fetch(uri + "/post/" + postId + "/comments", {
        method: "GET",
        credentials: "include",
        mode: "cors"
    });
    return comments.json()
};

export const getUserByUsername = async (username) => {
    return (await fetch(uri + "/user/name/" + username, {
        method: "GET",
        credentials: "include",
        mode: "cors"
    })).json()
};

export const getUserBySID = async () => {
    return (await fetch(uri + "/user/sid", {
        method: "GET",
        credentials: "include",
        mode: "cors"
    })).json();
};

export const logout = () => {
    fetch(uri + "/user/logout",
        {method: "POST", credentials: "include"}).then(() => window.location.reload());
};
