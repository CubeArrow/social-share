import React from "react"
import {MdThumbUp} from "react-icons/md"
import "../../css/buttons.scss"
const BackendConnector = require("../../../../utils/BackendConnector");


export default class CommentDisplay extends React.Component {
    render() {
        return (
            <div>
                <a href={"/" + this.props.value.userId.username}>{this.props.value.userId.username}</a> - {new Date(this.props.value.date).toLocaleString()}
                <p>{this.props.value.content}</p>
                <button className="likebutton" onClick={() => BackendConnector.likeComment(this.props.value._id)}>
                    {this.props.value.userLiked ? <MdThumbUp className="liked"/> : <MdThumbUp className="unliked"/>}
                    <span>{this.props.value.likesCount}</span>
                </button>
            </div>
        )
    }
}