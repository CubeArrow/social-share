import React from "react"
import Comment from "./Comment"
const BackendConnection = require("../../../../utils/BackendConnector");

export default class CommentDisplay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: null,
        };
    }

    componentDidMount() {
        BackendConnection.getComments(this.props.postId).then((comments) => this.setState({comments: comments}))
    }

    render() {
        if (!this.state.comments) {
            return <div style={{paddingTop: "100px"}}>
                <div className="spinner"/>
                <p>Loading ...</p></div>
        } else if (this.state.comments.message) {
            return <div style={{paddingTop: "100px"}}><h1>{this.state.comments.message}</h1></div>
        } else {
            if (this.state.comments !== "[]") {
                return (
                    <div>
                        {this.state.comments.map(comment => (
                            <Comment value={comment} key={comment._id}/>
                        ))}
                    </div>
                )
            } else {
                return (<div/>)
            }
        }
    }
}
