import React from "react"
import {Redirect} from "react-router-dom";
import {FaPlusSquare} from "react-icons/all";
import "./navbar.scss"
import Overlay from "./Overlay"
import PostCreator from "./PostCreator"
const BackendConnector = require("../../../../utils/BackendConnector");

class NavbarClass extends React.Component {
    constructor(props) {
        super(props);
        this.closePostCreator = this.closePostCreator.bind(this);

        this.state = {searchValue: "", startSearch: false, createPost: false};

    }

    closePostCreator = () => {
        this.setState({createPost: false})
    };

    render() {
        if (this.state.startSearch) {
            return <Redirect to={"/search/" + this.state.searchValue}/>
        }

        return (
            <div>
                <ul>
                    <li className="left">
                        <a href="/">SocialShare</a>
                    </li>

                    <li className="left">
                        <p onClick={() => this.setState({createPost: true})}><FaPlusSquare className="icon"/></p>
                    </li>

                    {this.props.currentUser ?
                        <li className="right">
                            <p className="activator">{this.props.currentUser.username}</p>
                            <div className="dropdown-content" id="userDropdown">
                                <a href="/account/settings">User Settings</a>
                                {this.props.currentUser.isAdmin && <a href="/admin/settings">Admin Settings</a>}

                                <p onClick={BackendConnector.logout}>logout</p>
                            </div>
                        </li>
                        :
                        <li className="right"><a href="/login/">Login</a></li>
                    }
                    <li className="right">
                        <form onSubmit={() => this.setState({startSearch: true})}>
                            <input type="text" placeholder="Search" className="mr-sm-2 formControl"
                                   onChange={(e) => this.setState({searchValue: e.target.value})}/>
                            <input type="submit" value="Search" id="searchButton"
                                   onClick={() => this.setState({startSearch: true})}/>
                        </form>
                    </li>
                </ul>
                {this.state.createPost &&
                <Overlay closeHandler={this.closePostCreator}>
                    <PostCreator closeHandler={this.closePostCreator}/>
                </Overlay>}
            </div>
        )
    }
}

export {NavbarClass}