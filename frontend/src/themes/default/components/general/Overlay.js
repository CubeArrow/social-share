import React from "react"
import "./overlay.scss"

export default class Overlay extends React.Component {
    close = (e, isOverlay) => {
        e.stopPropagation();
        if (!isOverlay) {
            document.getElementsByTagName("body")[0].style.overflowY = "scroll";

            this.props.closeHandler()
        }
    };

    constructor(props) {
        super(props);

        document.getElementsByTagName("body")[0].style.overflowY = "hidden"
    }

    render() {

        return (
            <div className="overlayWrapper" onClick={(e) => this.close(e, false)}>
                <div className="overlay" onClick={(e) => this.close(e, true)}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}