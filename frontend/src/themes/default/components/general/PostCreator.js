import React from "react"
import "./postCreator.scss"
const BackendConnector = require("../../../../utils/BackendConnector");

export default class PostCreator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {titleValue: "", file: ""};
    }

    submitPost = () => BackendConnector.submitPost(this.state.titleValue, this.state.file);

    updateTitle = (event) => {
        this.setState({titleValue: event.target.value})
    };
    updateFile = (event) => {
        this.setState({file: event.target.files[0]})
    };

    render() {
        return (
            <div>
                <h2>New Post</h2>
                <input placeholder="Title" type="input" id="postTitleInput" onChange={this.updateTitle}/>
                <div className="upload-btn-wrapper">
                    <button className="btn">Upload a file</button>
                    <input id="fileUpload" type="file" name="myfile" onChange={this.updateFile}/>
                </div>
                <br/>
                <button className="submitButton" onClick={this.submitPost}>Submit</button>
                <button className="cancelButton" onClick={this.props.closeHandler}>Cancel</button>

            </div>
        )
    }
}