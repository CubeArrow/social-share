import React from "react"
import UserDisplay from "../components/users/UserDisplay"
import {NavbarClass} from "../components/general/Navbar"
import {PostContainer} from "../components/posts/PostContainer"

const BackendConnector = require("../../../utils/BackendConnector");

export default class IndexPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            getPosts: false,
            user: null
        };
    }

    componentDidMount() {
        BackendConnector.getUserByUsername().then((user) => this.setState({user: user}));

    }

    render() {
        if (this.state.user) {
            return (
                <div>
                    <NavbarClass/>
                    <div className="index" style={{paddingTop: "80px"}}>
                        <UserDisplay user={this.state.user}/>
                        <PostContainer userId={this.state.user._id}/>
                    </div>
                </div>
            )
        }

        return (
            <div>
                <NavbarClass/>
                <div className="index" style={{paddingTop: "80px"}}>
                    <div className="spinner"/>
                    <p>Loading...</p>
                </div>
            </div>
        )
    }
}
