import React from "react"
import {BrowserRouter, Route, Switch} from "react-router-dom"
import {IndexPage} from "./pages/index"
import UserPage from "./pages/user"
import './css/index.scss';
import "./css/mediaQueries.css"
import "./css/buttons.scss"

const BackendConnector = require("../../utils/BackendConnector");



export default class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {user: null}
    }

    componentDidMount = () => {
        BackendConnector.getUserBySID().then((result) => this.setState({user: result}))
    };

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' render={() => <IndexPage currentUser={this.state.user}/>}/>
                    <Route exact path='/:user' render={() => <UserPage currentUser={this.state.user}/>}/>
                    <Route exact path="/search/:searchValue" render={() => <IndexPage currentUser={this.state.user}/>}/>
                    <Route exact path="/account/settings" render={() => <IndexPage currentUser={this.state.user}/>}/>
                </Switch>
            </BrowserRouter>
        )
    }
}